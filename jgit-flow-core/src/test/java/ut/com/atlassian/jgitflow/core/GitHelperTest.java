package ut.com.atlassian.jgitflow.core;

import java.util.List;

import javax.annotation.Nullable;

import com.atlassian.jgitflow.core.util.GitHelper;

import com.google.common.base.Function;
import com.google.common.collect.Iterables;

import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.lib.Constants;
import org.eclipse.jgit.lib.Ref;
import org.junit.Test;

import ut.com.atlassian.jgitflow.core.testutils.RepoUtil;

import static com.google.common.collect.Lists.newArrayList;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * @since version
 */
public class GitHelperTest extends BaseGitFlowTest
{
    @Test
    public void listBranchesWithPrefix() throws Exception
    {
        Git git = RepoUtil.createRepositoryWithMaster(newDir());
        git.branchCreate().setName("feature/my-feature").call();
        git.branchCreate().setName("feature/my-feature2").call();
        git.branchCreate().setName("release/1.1").call();
        
        List<Ref> branches = GitHelper.listBranchesWithPrefix(git,"feature/");
        
        List<String> names = newArrayList(Iterables.transform(branches, new Function<Ref, String>()
        {
            @Override
            public String apply(@Nullable Ref input)
            {
                return input.getName();
            }
        }));
        
        assertTrue(names.contains(Constants.R_HEADS + "feature/my-feature"));
        assertTrue(names.contains(Constants.R_HEADS + "feature/my-feature2"));
        assertFalse(names.contains(Constants.R_HEADS + "release/1.1"));
    }

    @Test
    public void localTagExists() throws Exception
    {
        Git git = RepoUtil.createRepositoryWithMaster(newDir());
        git.tag().setName("1.0").setMessage("tagged 1.0").call();

        assertTrue(GitHelper.tagExists(git,"1.0"));
    }

    @Test
    public void remoteTagExists() throws Exception
    {
        Git git = null;
        Git remoteGit = null;
        remoteGit = RepoUtil.createRepositoryWithMasterAndDevelop(newDir());
        git = Git.cloneRepository().setDirectory(newDir()).setURI("file://" + remoteGit.getRepository().getWorkTree().getPath()).call();
        
        remoteGit.tag().setName("1.0").setMessage("tagged 1.0").call();
        
        git.fetch().call();
        
        assertTrue(GitHelper.tagExists(git,"1.0"));
    }
}
