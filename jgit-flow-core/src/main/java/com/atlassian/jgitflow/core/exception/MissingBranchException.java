package com.atlassian.jgitflow.core.exception;

/**
 * @since version
 */
public class MissingBranchException extends JGitFlowException
{
    public MissingBranchException()
    {
    }

    public MissingBranchException(String message)
    {
        super(message);
    }

    public MissingBranchException(String message, Throwable cause)
    {
        super(message, cause);
    }

    public MissingBranchException(Throwable cause)
    {
        super(cause);
    }
}
