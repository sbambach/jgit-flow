package com.atlassian.jgitflow.core;

import java.io.IOException;
import java.util.List;
import java.util.Set;

import com.atlassian.jgitflow.core.util.GitHelper;

import com.google.common.collect.ImmutableList;

import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.lib.ConfigConstants;
import org.eclipse.jgit.lib.Constants;
import org.eclipse.jgit.lib.StoredConfig;

/**
 * @since version
 */
public class GitFlowConfiguration
{
    private final Git git;

    public GitFlowConfiguration(Git git)
    {
        this.git = git;
    }

    public String getDevelop()
    {
        return git.getRepository().getConfig().getString(JGitFlowConstants.SECTION, ConfigConstants.CONFIG_BRANCH_SECTION, JGitFlowConstants.DEVELOP_KEY);
    }

    public String getMaster()
    {
        return git.getRepository().getConfig().getString(JGitFlowConstants.SECTION, ConfigConstants.CONFIG_BRANCH_SECTION, Constants.MASTER);
    }

    public void setMaster(String branchName) throws IOException
    {
        StoredConfig config = git.getRepository().getConfig();
        config.setString(JGitFlowConstants.SECTION, ConfigConstants.CONFIG_BRANCH_SECTION, Constants.MASTER, branchName);
        config.save();
    }

    public void setDevelop(String branchName) throws IOException
    {
        StoredConfig config = git.getRepository().getConfig();
        config.setString(JGitFlowConstants.SECTION, ConfigConstants.CONFIG_BRANCH_SECTION, JGitFlowConstants.DEVELOP_KEY, branchName);
        config.save();
    }

    public boolean gitFlowIsInitialized() throws GitAPIException
    {
        return (hasMasterConfigured() && hasDevelopConfigured() && !getMaster().equals(getDevelop()) && hasPrefixesConfigured());
    }

    public boolean hasDevelopConfigured() throws GitAPIException
    {
        return GitHelper.localBranchExists(git, git.getRepository().getConfig().getString(JGitFlowConstants.SECTION, ConfigConstants.CONFIG_BRANCH_SECTION, JGitFlowConstants.DEVELOP_KEY));
    }

    public boolean hasMasterConfigured() throws GitAPIException
    {
        return GitHelper.localBranchExists(git, git.getRepository().getConfig().getString(JGitFlowConstants.SECTION, ConfigConstants.CONFIG_BRANCH_SECTION, Constants.MASTER));
    }

    

    public boolean hasPrefixesConfigured()
    {
        Set<String> entries = git.getRepository().getConfig().getNames(JGitFlowConstants.SECTION, JGitFlowConstants.PREFIX_SUB);
        
        return entries.containsAll(getPrefixNames());
    }
    
    public List<String> getPrefixNames()
    {
        return ImmutableList.of(
                JGitFlowConstants.PREFIXES.FEATURE.configKey()
                , JGitFlowConstants.PREFIXES.RELEASE.configKey()
                , JGitFlowConstants.PREFIXES.HOTFIX.configKey()
                , JGitFlowConstants.PREFIXES.SUPPORT.configKey()
                , JGitFlowConstants.PREFIXES.VERSIONTAG.configKey()
        );
    }

    public boolean hasPrefixConfigured(String prefixName)
    {
        Set<String> entries = git.getRepository().getConfig().getNames(JGitFlowConstants.SECTION, JGitFlowConstants.PREFIX_SUB);

        return entries.contains(prefixName);

    }

    public String getPrefixValue(String prefixName)
    {
        String val = git.getRepository().getConfig().getString(JGitFlowConstants.SECTION, JGitFlowConstants.PREFIX_SUB,prefixName);
        
        return (null != val) ? val : "";
    }

    public void setPrefix(String prefixName, String prefixValue) throws IOException
    {
        StoredConfig config = git.getRepository().getConfig();
        if(getPrefixNames().contains(prefixName))
        {
            config.setString(JGitFlowConstants.SECTION, JGitFlowConstants.PREFIX_SUB, prefixName, prefixValue);
            config.save();
        }
        
    }
}
