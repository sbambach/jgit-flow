package com.atlassian.jgitflow.core;

import com.atlassian.jgitflow.core.util.GitHelper;

import com.google.common.base.Strings;

import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.lib.Ref;
import org.eclipse.jgit.revwalk.RevCommit;

import static com.google.common.base.Preconditions.checkState;

/**
 * @since version
 */
public class HotfixStartCommand extends AbstractGitFlowCommand<Ref>
{
    private final String hotfixName;
    private boolean fetch;

    public HotfixStartCommand(String hotfixName, Git git, GitFlowConfiguration gfConfig)
    {
        super(git, gfConfig);

        checkState(!Strings.isNullOrEmpty(hotfixName));
        this.hotfixName = hotfixName;
        this.fetch = false;
    }
    
    @Override
    public Ref call() throws Exception
    {
        String prefixedHotfixName = gfConfig.getPrefixValue(JGitFlowConstants.PREFIXES.HOTFIX.configKey()) + hotfixName;
        
        requireGitFlowInitialized();
        requireNoExistingHotfixBranches();
        requireCleanWorkingTree();
        requireLocalBranchAbsent(prefixedHotfixName);

        if(fetch)
        {
            git.fetch().call();
        }
        
        requireTagAbsent(gfConfig.getPrefixValue(JGitFlowConstants.PREFIXES.VERSIONTAG.configKey()) + hotfixName);

        if(GitHelper.remoteBranchExists(git, gfConfig.getMaster()))
        {
            requireLocalBranchNotBehindRemote(gfConfig.getMaster());
        }

        RevCommit masterCommit = GitHelper.getLatestCommit(git,gfConfig.getMaster());

        return git.checkout()
                  .setName(prefixedHotfixName)
                  .setCreateBranch(true)
                  .setStartPoint(masterCommit)
                  .call();
    }

    public HotfixStartCommand setFetch(boolean fetch)
    {
        this.fetch = fetch;
        return this;
    }
}
