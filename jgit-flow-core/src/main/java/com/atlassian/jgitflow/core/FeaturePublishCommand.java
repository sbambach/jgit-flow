package com.atlassian.jgitflow.core;

import com.google.common.base.Strings;

import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.lib.ConfigConstants;
import org.eclipse.jgit.lib.Constants;
import org.eclipse.jgit.lib.StoredConfig;
import org.eclipse.jgit.transport.RefSpec;

import static com.google.common.base.Preconditions.checkState;

/**
 * @since version
 */
public class FeaturePublishCommand extends AbstractGitFlowCommand<Void>
{
    private final String branchName;
    
    public FeaturePublishCommand(String name, Git git, GitFlowConfiguration gfConfig)
    {
        super(git, gfConfig);
        checkState(!Strings.isNullOrEmpty(name));
        this.branchName = name;
    }

    @Override
    public Void call() throws Exception
    {
        requireGitFlowInitialized();
        String prefixedBranchName = gfConfig.getPrefixValue(JGitFlowConstants.PREFIXES.FEATURE.configKey()) + branchName;
        
        requireCleanWorkingTree();
        requireLocalBranchExists(prefixedBranchName);

        git.fetch().setRemote("origin").call();
        requireRemoteBranchAbsent(prefixedBranchName);

        //create remote feature branch
        RefSpec branchSpec = new RefSpec(prefixedBranchName + ":" + Constants.R_HEADS + prefixedBranchName);
        git.push().setRemote("origin").setRefSpecs(branchSpec).call();
        git.fetch().setRemote("origin").call();
        
        //setup tracking
        StoredConfig config = git.getRepository().getConfig();
        config.setString(ConfigConstants.CONFIG_BRANCH_SECTION,prefixedBranchName, ConfigConstants.CONFIG_KEY_REMOTE,"origin");
        config.setString(ConfigConstants.CONFIG_BRANCH_SECTION,prefixedBranchName, ConfigConstants.CONFIG_KEY_MERGE,Constants.R_HEADS + prefixedBranchName);
        config.save();
        
        //checkout the branch
        git.checkout().setName(prefixedBranchName).call();
        
        return null;
    }
}
