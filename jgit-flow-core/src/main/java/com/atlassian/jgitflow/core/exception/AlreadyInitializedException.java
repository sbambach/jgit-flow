package com.atlassian.jgitflow.core.exception;

/**
 * @since version
 */
public class AlreadyInitializedException extends JGitFlowException
{
    public AlreadyInitializedException()
    {
    }

    public AlreadyInitializedException(Throwable cause)
    {
        super(cause);
    }

    public AlreadyInitializedException(String message, Throwable cause)
    {
        super(message, cause);
    }

    public AlreadyInitializedException(String message)
    {
        super(message);
    }
}
