package com.atlassian.jgitflow.core.exception;

/**
 * @since version
 */
public class DirtyWorkingTreeException extends JGitFlowException
{

    public DirtyWorkingTreeException()
    {
    }

    public DirtyWorkingTreeException(String message)
    {
        super(message);
    }

    public DirtyWorkingTreeException(String message, Throwable cause)
    {
        super(message, cause);
    }

    public DirtyWorkingTreeException(Throwable cause)
    {
        super(cause);
    }
}
