package com.atlassian.jgitflow.core;

import com.atlassian.jgitflow.core.util.GitHelper;

import com.google.common.base.Strings;
import com.google.common.collect.ImmutableList;

import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.MergeCommand;
import org.eclipse.jgit.api.MergeResult;
import org.eclipse.jgit.lib.Constants;
import org.eclipse.jgit.lib.Ref;
import org.eclipse.jgit.revwalk.RevCommit;
import org.eclipse.jgit.transport.RefSpec;

import static com.google.common.base.Preconditions.checkState;

/**
 * @since version
 */
public class ReleaseFinishCommand extends AbstractGitFlowCommand<Void>
{
    private final String releaseName;
    private boolean fetch;
    private String message;
    private boolean push;
    private boolean keepBranch;
    private boolean noTag;
    private boolean squash;

    public ReleaseFinishCommand(String releaseName, Git git, GitFlowConfiguration gfConfig)
    {
        super(git, gfConfig);
        checkState(!Strings.isNullOrEmpty(releaseName));
        this.releaseName = releaseName;
        this.fetch = false;
        this.message = "tagging release " + releaseName;
        this.push = false;
        this.keepBranch = false;
        this.noTag = false;
        this.squash = false;
    }

    @Override
    public Void call() throws Exception
    {
        String prefixedReleaseName = gfConfig.getPrefixValue(JGitFlowConstants.PREFIXES.RELEASE.configKey()) + releaseName;

        requireLocalBranchExists(prefixedReleaseName);
        requireCleanWorkingTree();

        if (fetch)
        {
            RefSpec developSpec = new RefSpec("+" + Constants.R_HEADS + gfConfig.getDevelop() + ":" + Constants.R_REMOTES + "origin/" + gfConfig.getDevelop());
            RefSpec masterSpec = new RefSpec("+" + Constants.R_HEADS + gfConfig.getMaster() + ":" + Constants.R_REMOTES + "origin/" + gfConfig.getMaster());

            git.fetch().setRemote(Constants.DEFAULT_REMOTE_NAME).setRefSpecs(masterSpec).call();
            git.fetch().setRemote(Constants.DEFAULT_REMOTE_NAME).setRefSpecs(developSpec).call();
            git.fetch().setRemote(Constants.DEFAULT_REMOTE_NAME).call();
        }

        if (GitHelper.remoteBranchExists(git, gfConfig.getMaster()))
        {
            requireLocalBranchNotBehindRemote(gfConfig.getMaster());
        }

        if (GitHelper.remoteBranchExists(git, gfConfig.getDevelop()))
        {
            requireLocalBranchNotBehindRemote(gfConfig.getDevelop());
        }

        Ref releaseBranch = GitHelper.getLocalBranch(git, prefixedReleaseName);
        RevCommit releaseCommit = GitHelper.getLatestCommit(git,prefixedReleaseName);
        
        /*
        try to merge into master
        in case a previous attempt to finish this release branch has failed,
        but the merge into master was successful, we skip it now
         */
        if (!GitHelper.isMergedInto(git, prefixedReleaseName, gfConfig.getMaster()))
        {
            git.checkout().setName(gfConfig.getMaster()).call();

            MergeResult masterResult = null;

            if (squash)
            {
                masterResult = git.merge().setSquash(true).include(releaseBranch).call();
                git.commit().call();
            }
            else
            {
                masterResult = git.merge().setFastForward(MergeCommand.FastForwardMode.NO_FF).include(releaseBranch).call();
            }
        }

        if (!noTag)
        {
            /*
            try to tag the release
            in case a previous attempt to finish this release branch has failed,
            but the tag was successful, we skip it now
            */
            String tagName = gfConfig.getPrefixValue(JGitFlowConstants.PREFIXES.VERSIONTAG.configKey()) + releaseName;
            if(!GitHelper.tagExists(git,tagName))
            {
                git.tag().setName(tagName).setMessage(message).setObjectId(releaseCommit).call();
            }
        }
        
        /*
        try to merge into develop
        in case a previous attempt to finish this release branch has failed,
        but the merge into develop was successful, we skip it now
         */
        if (!GitHelper.isMergedInto(git, prefixedReleaseName, gfConfig.getDevelop()))
        {
            git.checkout().setName(gfConfig.getDevelop()).call();

            MergeResult developResult = null;

            if (squash)
            {
                developResult = git.merge().setSquash(true).include(releaseBranch).call();
                git.commit().call();
            }
            else
            {
                developResult = git.merge().setFastForward(MergeCommand.FastForwardMode.NO_FF).include(releaseBranch).call();
            }
        }
        
        if(!keepBranch)
        {
            //make sure we're on master
            git.checkout().setName(gfConfig.getMaster()).call();
            git.branchDelete().setForce(true).setBranchNames(prefixedReleaseName).call();
        }
        
        if(push)
        {
            //push to develop
            RefSpec developSpec = new RefSpec(gfConfig.getDevelop());
            git.push().setRemote(Constants.DEFAULT_REMOTE_NAME).setRefSpecs(developSpec).call();
            
            //push to master
            RefSpec masterSpec = new RefSpec(gfConfig.getMaster());
            git.push().setRemote(Constants.DEFAULT_REMOTE_NAME).setRefSpecs(masterSpec).call();
            
            if(!noTag)
            {
                git.push().setRemote(Constants.DEFAULT_REMOTE_NAME).setPushTags().call();
            }

            if (GitHelper.remoteBranchExists(git, prefixedReleaseName))
            {
                RefSpec branchSpec = new RefSpec(prefixedReleaseName);
                git.push().setRemote(Constants.DEFAULT_REMOTE_NAME).setRefSpecs(branchSpec).call();
            }
        }

        git.checkout().setName(gfConfig.getDevelop()).call();
        
        return null;
    }

    public ReleaseFinishCommand setFetch(boolean fetch)
    {
        this.fetch = fetch;
        return this;
    }

    public ReleaseFinishCommand setMessage(String message)
    {
        this.message = message;
        return this;
    }

    public ReleaseFinishCommand setPush(boolean push)
    {
        this.push = push;
        return this;
    }

    public ReleaseFinishCommand setKeepBranch(boolean keepBranch)
    {
        this.keepBranch = keepBranch;
        return this;
    }

    public ReleaseFinishCommand setNoTag(boolean noTag)
    {
        this.noTag = noTag;
        return this;
    }

    public ReleaseFinishCommand setSquash(boolean squash)
    {
        this.squash = squash;
        return this;
    }
}
