package com.atlassian.jgitflow.core.exception;

/**
 * @since version
 */
public class BranchOutOfDateException extends JGitFlowException
{
    public BranchOutOfDateException()
    {
    }

    public BranchOutOfDateException(String message)
    {
        super(message);
    }

    public BranchOutOfDateException(String message, Throwable cause)
    {
        super(message, cause);
    }

    public BranchOutOfDateException(Throwable cause)
    {
        super(cause);
    }
}
