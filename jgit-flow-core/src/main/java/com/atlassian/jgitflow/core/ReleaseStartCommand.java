package com.atlassian.jgitflow.core;

import com.atlassian.jgitflow.core.util.GitHelper;

import com.google.common.base.Strings;

import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.lib.Ref;
import org.eclipse.jgit.revwalk.RevCommit;

import static com.google.common.base.Preconditions.checkState;

/**
 * @since version
 */
public class ReleaseStartCommand extends AbstractGitFlowCommand<Ref>
{
    //TODO: add ability to pass in start commit on ALL commands
    private final String releaseName;
    private boolean fetch;
    
    public ReleaseStartCommand(String releaseName, Git git, GitFlowConfiguration gfConfig)
    {
        super(git, gfConfig);
        
        checkState(!Strings.isNullOrEmpty(releaseName));
        this.releaseName = releaseName;
        this.fetch = false;
    }

    @Override
    public Ref call() throws Exception
    {
        String prefixedReleaseName = gfConfig.getPrefixValue(JGitFlowConstants.PREFIXES.RELEASE.configKey()) + releaseName;
        
        requireGitFlowInitialized();
        requireNoExistingReleaseBranches();
        requireCleanWorkingTree();
        requireLocalBranchAbsent(prefixedReleaseName);
        
        if(fetch)
        {
            git.fetch().call();    
        }
        
        requireTagAbsent(gfConfig.getPrefixValue(JGitFlowConstants.PREFIXES.VERSIONTAG.configKey()) + releaseName);

        if(GitHelper.remoteBranchExists(git, gfConfig.getDevelop()))
        {
            requireLocalBranchNotBehindRemote(gfConfig.getDevelop());
        }

        RevCommit developCommit = GitHelper.getLatestCommit(git,gfConfig.getDevelop());
        
        return git.checkout()
           .setName(prefixedReleaseName)
           .setCreateBranch(true)
           .setStartPoint(developCommit)
           .call();
    }

    public ReleaseStartCommand setFetch(boolean fetch)
    {
        this.fetch = fetch;
        return this;
    }
}
