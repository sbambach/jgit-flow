package com.atlassian.jgitflow.core;

import com.google.common.base.Strings;

import org.eclipse.jgit.api.Git;

import static com.google.common.base.Preconditions.checkState;

/**
 * @since version
 */
public class FeatureRebaseCommand extends AbstractGitFlowCommand<Void>
{
    private final String branchName;
    
    public FeatureRebaseCommand(String name, Git git, GitFlowConfiguration gfConfig)
    {
        super(git, gfConfig);
        checkState(!Strings.isNullOrEmpty(name));
        this.branchName = gfConfig.getPrefixValue(JGitFlowConstants.PREFIXES.FEATURE.configKey()) + name;
    }

    @Override
    public Void call() throws Exception
    {
        requireGitFlowInitialized();
        requireCleanWorkingTree();
        requireLocalBranchExists(branchName);
        
        git.checkout().setName(branchName).call();
        git.rebase().call();
        
        return null;
    }
}
