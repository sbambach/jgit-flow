package com.atlassian.jgitflow.core;

import java.io.File;
import java.io.IOException;

import com.atlassian.jgitflow.core.exception.JGitFlowException;

import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.errors.GitAPIException;

/**
 * @since version
 */
public class JGitFlow
{
    private Git git;
    private GitFlowConfiguration gfConfig;

    private JGitFlow()
    {
    }
    
    JGitFlow(Git git, GitFlowConfiguration gfConfig)
    {
        this.git = git;
        this.gfConfig = gfConfig;
    }

    public static JGitFlow init(File tempDir) throws JGitFlowException
    {
        JGitFlowInitCommand initCommand = new JGitFlowInitCommand();
        return initCommand.setDirectory(tempDir).call();
    }

    public static JGitFlow init(File tempDir, InitContext context) throws JGitFlowException
    {
        JGitFlowInitCommand initCommand = new JGitFlowInitCommand();
        return initCommand.setDirectory(tempDir).setInitContext(context).call();
    }

    public static JGitFlow forceInit(File tempDir) throws JGitFlowException
    {
        JGitFlowInitCommand initCommand = new JGitFlowInitCommand();
        return initCommand.setDirectory(tempDir).setForce(true).call();
    }

    public static JGitFlow forceInit(File tempDir, InitContext context) throws JGitFlowException
    {
        JGitFlowInitCommand initCommand = new JGitFlowInitCommand();
        return initCommand.setDirectory(tempDir).setForce(true).setInitContext(context).call();
    }

    public static JGitFlow get(File tempDir) throws JGitFlowException
    {
        try
        {
            Git gitRepo = Git.open(tempDir);
            GitFlowConfiguration gfConfig = new GitFlowConfiguration(gitRepo);
            return new JGitFlow(gitRepo,gfConfig);
        }
        catch (IOException e)
        {
            throw new JGitFlowException(e);
        }
    }

    public static JGitFlow getOrInit(File tempDir) throws JGitFlowException
    {
        if(isInitialized(tempDir))
        {
            return get(tempDir);
        }
        else
        {
            return init(tempDir);
        }
    }

    public static JGitFlow getOrInit(File tempDir,InitContext ctx) throws JGitFlowException
    {
        if(isInitialized(tempDir))
        {
            return get(tempDir);
        }
        else
        {
            return init(tempDir,ctx);
        }
    }
    
    public static boolean isInitialized(File dir)
    {
        boolean inited = false;
        try
        {
            Git gitRepo = Git.open(dir);
            GitFlowConfiguration gfConfig = new GitFlowConfiguration(gitRepo);
            inited = gfConfig.gitFlowIsInitialized();
        }
        catch (IOException e)
        {
            //ignore
        }
        catch (GitAPIException e)
        {
            //ignore
        }

        return inited;
    }

    public boolean isInitialized()
    {
        boolean inited = false;
        try
        {
            inited = gfConfig.gitFlowIsInitialized();
        }
        catch (GitAPIException e)
        {
           //ignore
        }
        
        return inited;
    }
    
    public FeatureStartCommand featureStart(String name)
    {
        return new FeatureStartCommand(name,git,gfConfig);
    }

    public FeatureFinishCommand featureFinish(String name)
    {
        return new FeatureFinishCommand(name,git,gfConfig);
    }

    public FeaturePublishCommand featurePublish(String name)
    {
        return new FeaturePublishCommand(name,git,gfConfig);
    }

    public ReleaseStartCommand releaseStart(String name)
    {
        return new ReleaseStartCommand(name,git,gfConfig);
    }
    
    public ReleaseFinishCommand releaseFinish(String name)
    {
        return new ReleaseFinishCommand(name,git,gfConfig);
    }

    public HotfixStartCommand hotfixStart(String name)
    {
        return new HotfixStartCommand(name,git,gfConfig);
    }

    public HotfixFinishCommand hotfixFinish(String name)
    {
        return new HotfixFinishCommand(name,git,gfConfig);
    }
    
    public Git git()
    {
        return git;    
    }
    
    public String getMasterBranchName()
    {
        return gfConfig.getMaster();
    }

    public String getDevelopBranchName()
    {
        return gfConfig.getDevelop();
    }

    public String getFeatureBranchPrefix()
    {
        return gfConfig.getPrefixValue(JGitFlowConstants.PREFIXES.FEATURE.configKey());
    }

    public String getReleaseBranchPrefix()
    {
        return gfConfig.getPrefixValue(JGitFlowConstants.PREFIXES.RELEASE.configKey());
    }

    public String getHotfixBranchPrefix()
    {
        return gfConfig.getPrefixValue(JGitFlowConstants.PREFIXES.HOTFIX.configKey());
    }

    public String getSupportBranchPrefix()
    {
        return gfConfig.getPrefixValue(JGitFlowConstants.PREFIXES.SUPPORT.configKey());
    }

    public String getVersionTagPrefix()
    {
        return gfConfig.getPrefixValue(JGitFlowConstants.PREFIXES.VERSIONTAG.configKey());
    }

}
