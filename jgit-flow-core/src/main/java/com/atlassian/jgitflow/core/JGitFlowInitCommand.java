package com.atlassian.jgitflow.core;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.Callable;

import com.atlassian.jgitflow.core.exception.AlreadyInitializedException;
import com.atlassian.jgitflow.core.exception.JGitFlowException;
import com.atlassian.jgitflow.core.util.GitHelper;

import org.eclipse.jgit.api.CreateBranchCommand;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.errors.IncorrectObjectTypeException;
import org.eclipse.jgit.errors.MissingObjectException;
import org.eclipse.jgit.errors.RepositoryNotFoundException;
import org.eclipse.jgit.lib.Constants;
import org.eclipse.jgit.lib.ObjectId;
import org.eclipse.jgit.lib.RefUpdate;
import org.eclipse.jgit.lib.Repository;
import org.eclipse.jgit.revwalk.RevCommit;
import org.eclipse.jgit.revwalk.RevWalk;

/**
 * @since version
 */
public class JGitFlowInitCommand implements Callable<JGitFlow>
{
    private File directory;
    private boolean force;
    private InitContext context;

    public JGitFlowInitCommand()
    {
        this.force = false;
    }

    @Override
    public JGitFlow call() throws JGitFlowException
    {
        Git git = null;

        if (null == this.context)
        {
            this.context = new InitContext();
        }

        try
        {
            git = getOrInitGit(directory);
        }
        catch (IOException e)
        {
            throw new JGitFlowException(e);
        }
        catch (GitAPIException e)
        {
            throw new JGitFlowException(e);
        }

        Repository repo = git.getRepository();
        GitFlowConfiguration gfConfig = new GitFlowConfiguration(git);
        RevWalk walk = null;
        try
        {
            if (!force && gfConfig.gitFlowIsInitialized())
            {
                throw new AlreadyInitializedException("Already initialized for git flow.");
            }

            //First setup master
            if (gfConfig.hasMasterConfigured() && !force)
            {
                context.setMaster(gfConfig.getMaster());
            }
            else
            {
                //if no local master exists, but a remote does, check it out
                if (!GitHelper.localBranchExists(git, context.getMaster()) && GitHelper.remoteBranchExists(git, context.getMaster()))
                {
                    git.branchCreate()
                       .setName(context.getMaster())
                       .setUpstreamMode(CreateBranchCommand.SetupUpstreamMode.SET_UPSTREAM)
                       .setStartPoint("origin/" + context.getMaster())
                       .call();
                }
            }

            gfConfig.setMaster(context.getMaster());


            //now setup develop
            if (gfConfig.hasDevelopConfigured() && !force)
            {
                context.setDevelop(gfConfig.getDevelop());
            }

            if (context.getDevelop().equals(context.getMaster()))
            {
                throw new JGitFlowException("master and develop branches cannot be the same: [" + context.getMaster() + "]");
            }

            gfConfig.setDevelop(context.getDevelop());

            //Creation of HEAD
            walk = new RevWalk(repo);
            ObjectId masterBranch = repo.resolve(Constants.R_HEADS + context.getMaster());
            RevCommit masterCommit = null;

            if (null != masterBranch)
            {
                try
                {
                    masterCommit = walk.parseCommit(masterBranch);
                }
                catch (MissingObjectException e)
                {
                    //ignore
                }
                catch (IncorrectObjectTypeException e)
                {
                    //ignore
                }
            }

            if (null == masterCommit)
            {
                RefUpdate refUpdate = repo.getRefDatabase().newUpdate(Constants.HEAD, false);
                refUpdate.setForceUpdate(true);
                refUpdate.link(Constants.R_HEADS + context.getMaster());

                git.commit().setMessage("Initial Commit").call();
            }

            //creation of develop
            if (!GitHelper.localBranchExists(git, context.getDevelop()))
            {
                if (GitHelper.remoteBranchExists(git, context.getDevelop()))
                {
                    git.branchCreate()
                       .setName(context.getDevelop())
                       .setUpstreamMode(CreateBranchCommand.SetupUpstreamMode.SET_UPSTREAM)
                       .setStartPoint("origin/" + context.getDevelop())
                       .call();
                }
                else
                {
                    git.branchCreate()
                       .setName(context.getDevelop())
                       .setUpstreamMode(CreateBranchCommand.SetupUpstreamMode.NOTRACK)
                       .call();
                }
            }
            git.checkout().setName(context.getDevelop()).call();

            //setup prefixes
            for (String prefixName : gfConfig.getPrefixNames())
            {
                if (!gfConfig.hasPrefixConfigured(prefixName) || force)
                {
                    gfConfig.setPrefix(prefixName, context.getPrefix(prefixName));
                }
            }

        }
        catch (IOException e)
        {
            throw new JGitFlowException(e);
        }
        catch (GitAPIException e)
        {
            throw new JGitFlowException(e);
        }
        finally
        {
            if (null != walk)
            {
                walk.release();
            }
        }

        return new JGitFlow(git, gfConfig);
    }


    public JGitFlowInitCommand setDirectory(File directory)
    {
        this.directory = directory;
        return this;
    }

    public JGitFlowInitCommand setInitContext(InitContext context)
    {
        this.context = context;
        return this;
    }

    public JGitFlowInitCommand setForce(boolean force)
    {
        this.force = force;
        return this;
    }

    private Git getOrInitGit(File folder) throws IOException, GitAPIException
    {
        Git gitRepo;
        try
        {
            gitRepo = Git.open(folder);
        }
        catch (RepositoryNotFoundException e)
        {
            gitRepo = Git.init().setDirectory(folder).call();
        }

        return gitRepo;
    }


}
