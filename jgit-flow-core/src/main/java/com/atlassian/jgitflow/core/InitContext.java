package com.atlassian.jgitflow.core;

import com.google.common.base.Strings;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Preconditions.checkState;

/**
 * @since version
 */
public class InitContext
{
    private String master;
    private String develop;
    private String feature;
    private String release;
    private String hotfix;
    private String support;
    private String versiontag;

    public InitContext()
    {
        this.master = "master";
        this.develop = "develop";
        this.feature = "feature/";
        this.release = "release/";
        this.hotfix = "hotfix/";
        this.support = "support/";
        this.versiontag = "";
    }

    public InitContext setMaster(String master)
    {
        checkState(!Strings.isNullOrEmpty(master));
        
        this.master = master;
        return this;
    }

    public InitContext setDevelop(String develop)
    {
        checkState(!Strings.isNullOrEmpty(develop));
        this.develop = develop;
        return this;
    }

    public InitContext setFeature(String feature)
    {
        checkState(!Strings.isNullOrEmpty(feature));
        this.feature = feature;
        return this;
    }

    public InitContext setRelease(String release)
    {
        checkState(!Strings.isNullOrEmpty(release));
        this.release = release;
        return this;
    }

    public InitContext setHotfix(String hotfix)
    {
        checkState(!Strings.isNullOrEmpty(hotfix));
        this.hotfix = hotfix;
        return this;
    }

    public InitContext setSupport(String support)
    {
        checkState(!Strings.isNullOrEmpty(support));
        this.support = support;
        return this;
    }

    public InitContext setVersiontag(String versiontag)
    {
        checkNotNull(versiontag);
        
        this.versiontag = versiontag;
        return this;
    }

    public String getMaster()
    {
        return master;
    }

    public String getDevelop()
    {
        return develop;
    }

    public String getFeature()
    {
        return feature;
    }

    public String getRelease()
    {
        return release;
    }

    public String getHotfix()
    {
        return hotfix;
    }

    public String getSupport()
    {
        return support;
    }

    public String getVersiontag()
    {
        return versiontag;
    }

    public void setPrefix(String prefixName, String prefixValue)
    {
        JGitFlowConstants.PREFIXES prefix = JGitFlowConstants.PREFIXES.valueOf(prefixName.toUpperCase());
        switch (prefix)
        {
            case FEATURE:
                setFeature(prefixValue);
                break;
            case RELEASE:
                setRelease(prefixValue);
                break;
            case HOTFIX:
                setHotfix(prefixValue);
                break;
            case SUPPORT:
                setSupport(prefixValue);
                break;
            case VERSIONTAG:
                setVersiontag(prefixValue);
                break;
        }
    }

    public String getPrefix(String prefixName)
    {
        JGitFlowConstants.PREFIXES prefix = JGitFlowConstants.PREFIXES.valueOf(prefixName.toUpperCase());
        String value = null;
        switch (prefix)
        {
            case FEATURE:
                value = getFeature();
                break;
            case RELEASE:
                value = getRelease();
                break;
            case HOTFIX:
                value = getHotfix();
                break;
            case SUPPORT:
                value = getSupport();
                break;
            case VERSIONTAG:
                value = getVersiontag();
                break;
        }
        
        return value;
    }
}