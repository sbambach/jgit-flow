package com.atlassian.jgitflow.core.exception;

/**
 * @since version
 */
public class BranchExistsException extends JGitFlowException
{
    public BranchExistsException()
    {
    }

    public BranchExistsException(String message)
    {
        super(message);
    }

    public BranchExistsException(String message, Throwable cause)
    {
        super(message, cause);
    }

    public BranchExistsException(Throwable cause)
    {
        super(cause);
    }
}
