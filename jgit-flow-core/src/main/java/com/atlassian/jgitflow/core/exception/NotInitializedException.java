package com.atlassian.jgitflow.core.exception;

/**
 * @since version
 */
public class NotInitializedException extends JGitFlowException
{
    public NotInitializedException()
    {
    }

    public NotInitializedException(String message)
    {
        super(message);
    }

    public NotInitializedException(String message, Throwable cause)
    {
        super(message, cause);
    }

    public NotInitializedException(Throwable cause)
    {
        super(cause);
    }
}
